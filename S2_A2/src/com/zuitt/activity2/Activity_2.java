package com.zuitt.activity2;
import java.util.ArrayList;
import java.util.HashMap;

public class Activity_2 {
  public static void main(String[] args) {
    int[] primeNumbers = {2, 3, 5, 7, 11};
    System.out.println("The first prime number is: " + primeNumbers[0]);
    System.out.println("The second prime number is: " + primeNumbers[1]);
    System.out.println("The third prime number is: " + primeNumbers[2]);
    System.out.println("The fourth prime number is: " + primeNumbers[3]);
    System.out.println("The fifth prime number is: " + primeNumbers[4]);

    // Array List
    ArrayList<String> friends = new ArrayList<>();
    friends.add("John");
    friends.add("Jane");
    friends.add("Chloe");
    friends.add("Zoey");
    System.out.println("My friends are: " + friends);

    // HashMaps
    HashMap<String, Integer> inventory = new HashMap<>();
    inventory.put("toothpaste", 15);
    inventory.put("toothbrush", 20);
    inventory.put("soap", 12);
    System.out.println("Our current inventory consists of: " + inventory);
  }
}
